from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from .acls import pictures, get_weather_data
import json

class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url"
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
        #"weath_data"
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated"
    ]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        #inst resp var set to an empty list
        #?we can use list comp like in api_list_presentations
        #*response = []
        #*we will update this to use the encoder
        #get all conference objects and set to conf var
        conferences = Conference.objects.all()
        #?create a resp var for our pic urls set to []
        #iterate over all conference objects
        # return JsonResponse(
        #     #!transfered to the encoder<<<
        # )
        #!after it gets related to the href, it will return a dict with the key "conferences"
        #?i changed the val to conferences from response
        #?that will pull the data from conferences and do the for loop in the json file
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.filter(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"error": "Location does not exist"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )




@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":

        #get the single conf instance by id
        conference = Conference.objects.get(id=id)
        #*weather data assigned to a var route: city and state
        weather = get_weather_data(
            conference.location.city,
            conference.location.state.abbreviation,
            )
        #return a dict with the conference's data
        #and the weather data
        #?the keys are in the Conf model
        return JsonResponse(
            {"weather": weather, "conference": conference},
            #conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"error": "Conference does not exist"},
            )
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )



@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        #get all location objects and set to loc var
        locations = Location.objects.all()
        #!after it gets related to the href,
        #!it will return a dict with the key "locations"
        return JsonResponse(
            #?i changed the val to conferences from response
            #?that will pull the data from conferences
            #?and do the for loop in the json file
            #{"locations": locations},
            locations,
            encoder=LocationListEncoder,
            safe=False,
            )
    else:
        #try to get the data from the request
        #set cont var to a json form str in req.body
        content = json.loads(request.body)
        try:
            #get the State object and put it in the content dict
            state = State.objects.get(abbreviation=content["state"])
            #set the content val as state
            content["state"] = state
        except State.DoesNotExist:
            #return json resp with error message
            return JsonResponse(
                {"error": "State abbrv does not exist"},
                status=400,
            )
        #*use city/state abbreviation in the cont dict to get the pho url
        #*to call the get_photo ACL function
        photo = pictures(content["city"], content["state"].abbreviation)
        content.update({"picture_url": photo})


        #*update the cont dict

        #set loc var to create a
        location = Location.objects.create(**content)
        #return a json resp using the data at loc and LDE
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        #get the single loc instance by id
        location = Location.objects.get(id=id)
        #return a dict with the location's data
        #?the keys are in the Location model
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        #set count as first param, generic as second
        #then filter and delete using id
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        #load json data from request body into var
        content = json.loads(request.body)
        try:
            #if state exists
            if "state" in content:
                #turn it into a State object
                state = State.objects.get(abbreviation=content["state"])
                #set cont['state] to state
                content["state"] = state
        except State.DoesNotExist:
            #return json resp with error message
            return JsonResponse(
                {"error": "State does not exist"},
                status=400,
            )
        #set loc obj with the ass id to update
        Location.objects.filter(id=id).update(**content)
        #return a json resp using the data at loc and LDE
        return JsonResponse(
            Location.objects.get(id=id),
            encoder=LocationDetailEncoder,
            safe=False,
        )
