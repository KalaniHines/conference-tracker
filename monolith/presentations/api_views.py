from django.http import JsonResponse
from .models import Presentation #, Status
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder, Conference
from django.views.decorators.http import require_http_methods
import json
import pika


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference"
    ]
    #added the encoder and conference from above.
    #this populates show pres det
    encoders = {
        "conference": ConferenceListEncoder(),
    }
    def get_extra_data(self, o):
        return {"status": o.status.name}

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    #!the other data is covered by extra data and the model
    properties = [
        "title",
        #"status",
        #!does below work
        #*yes, still not sending email. Have the field though
        "presenter_name",
        "presenter_email"
        #?href comes from the model, we dont need it here
        #"href",
    ]
    #Hey, look. We got the status of the presentation!
    def get_extra_data(self, o):
        return {"status": o.status.name}

#create a func that will send the rabbit message for the queue
def send_message(queue_name, body):
    #?create a connection to rabbitMQ using the pika python library
    #set a param var to connection params with the host rabbitmq
    parameters = pika.ConnectionParameters(host="rabbitmq")
    #inst a conn var that establishes a connection to rabbit using th params
    connection = pika.BlockingConnection(parameters)
    #create a channel using the connect channel method, this is the coms between clients and RMQ
    channel = connection.channel()
    ##call q decl to create a new q if it doesnt exist, pass q name to specify the q
    channel.queue_declare(queue=queue_name)
    #call bas pub on channel to pub the message
    channel.basic_publish(
        #empty '' to indicate a dir mess
        exchange='',
        #send the message dir to RK
        routing_key=queue_name,
        #serialize the body
        body=json.dumps(body),
    )
    connection.close()




@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        #added filter, changed from  all()
        presentations = Presentation.objects.filter(conference=conference_id)
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            # presentations = Presentation.objects.filter(id=conference_id)
            # content["presentations"] = presentations
            #!revisit how this comes from Conference
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"error": "Presentation does not exist"},
                status=400,
            )
        # presentation = Presentation.objects.create(**content)
        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    #set pres var to the Presentation obj's related to the id
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        try:
            if "presentation" in content:
                presentation = Presentation.objects.get(["presentation"])
                content["presentation"] = presentation
        except Presentation.DoesNotExist:
            return JsonResponse(
                {"error": "Presentation does not exist"},
            )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )

#create a couple of views that can handle approvals and rejections.
#*These use the methods of the Presentation
#*acting as the root of the Presentation-Status aggregate.
@require_http_methods(["PUT"])
def api_approve_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.approve()
    body = {
        #!set the feilds to the pres. approval status
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
        }
    send_message("presentation_approvals", body)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_reject_presentation(request, pk):
    presentation = Presentation.objects.get(id=pk)
    presentation.reject()
    body = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
    }
    send_message("presentation_rejections", body)
    return JsonResponse(
        presentation,
        encoder=PresentationDetailEncoder,
        safe=False,
    )
