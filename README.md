# Your Go-to Conference Tracker!
Built and maintained by Kalani Hines

Each step of this project I created dev branches:
starting from 1 up until 7 where I moved to the front end repository.
Link below and in my resume as pt. 2

## I have done step by step work in different dev branches
1. Main is where I merge each dev branch once its stable
2. pre-RESTful is my initial work branch.
3. Encoders is the branch that I added all of the encoders in my views
4. RESTful is the branch that I added the http requirements for Get, Post, Put and Delete
5. Docker is the branch that I added to get my project out of the built in .venv and move to Docker.
   This is the beginning of the project going from Monolithic to MicroService
6. weather branch is where I added the anti corruption layers for Pexels and Weather API keys. I used 3rd party data to assign pictures and weather data to my show_conference and list_locations API's.
7. micro is my branch where I will unmonolith my project. I will also start to take out my pseudocode and docsstrings to clean it up. The pseudo will still be in my dev brances up to weather.

At this point I move into the front end with a new project stub:
https://gitlab.com/KalaniHines/conference-tracker-fe
