from django.http import JsonResponse
from .models import Attendee, ConferenceVO, AccountVO
from common.json import ModelEncoder
#!from events.api_views import ConferenceListEncoder
from django.views.decorators.http import require_http_methods
#!from events.models import Conference
import json


#new value obj encoder for the micro
class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        ]

class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceVODetailEncoder(),
    }

    def get_extra_data(self, o):
        # extra_data = super().get_extra_data(o)
        # if extra_data.get("email") == o.email:
        #     extra_data["has_account"] = True
        # else:
        #     extra_data["has_account"] = False
        # return extra_data
        count = AccountVO.objects.filter(email=o.email).count()
        return {"has_account": count > 0}



@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        #response = []
        #get all of the attendee objects and set them to a attendees var
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
            )
    else:
        content = json.loads(request.body)
        try:
            #add the href and the path of the conference
            conference_href = f'/api/conferences/{conference_vo_id}/'
            #change the conf var
            #conference = Conference.objects.get(id=conference_id)
            #import the href from the model VO
            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
            {"error": "Invalid conference id"},
            status=400,
        )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )




@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})
    else:
        content = json.loads(request.body)
        try:
            if "attendee" in content:
                attendee = Attendee.objects.get(["attendee"])
                content["attendee"] = attendee
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"error": "Invalid attendee id"},
                status=400,
            )
        Attendee.objects.filter(id=id).update(**content)
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
