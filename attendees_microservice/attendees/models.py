from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist


#this val obj model will replace the old Conf model
#associated to another bounded context
class ConferenceVO(models.Model):
    #the href var will be the association we use
    #as we wont have the id to call the data
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class AccountVO(models.Model):
    email = models.EmailField()
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    is_active = models.BooleanField(default=True)
    updated = models.DateTimeField(auto_now_add=True)


class Attendee(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    conference = models.ForeignKey(
        # "events.Conference",
        #replace above with the new ConfVO
        ConferenceVO,
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def create_badge(self):
        try:
            #do nothing if the attendee inst already has a badge
            self.badge
        except ObjectDoesNotExist:
            #if Obj does not exist, create a badge inst
            #with self as val for the attendee prop of the badge
            Badge.objects.create(attendee=self)
            self.save()

    def __str__(self):
        return self.name


    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"id": self.id})


class Badge(models.Model):
    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )
